<?php

function subset_db_drush_command() {
  $items = array();
  $items['delete-old-nodes'] = array(
    'callback' => 'subset_db_delete_nodes',  // Callback function
    'description' => 'Drush command to delete all nodes older than a particular date.',
    'aliases' => array('don'), // alias of command
    'examples' => array(      // List these example when user types : drush help nc
      'Delete nodes older than a date' => 'drush don yyyy-mm-dd',
    ),
  );
  return $items;
}

/*
 * Callback function for hook_drush_command().
 */
function subset_db_delete_nodes() {
  // Get arguments passed in command, Ex: drush don date
  # TODO add "dry run" that returns the count of affected nodes only
  $args = func_get_args();
  if ((count($args) > 0) && is_array($args)) {
    $date = array_shift($args);
    $timestamp = strtotime($date);

    #TODO validate that the date is actually a date (https://www.codexworld.com/how-to/validate-date-input-string-in-php/)
    if(validateDate($date)) {
      drush_print('deleting older than '.$date);

      $result = db_select('node', 'n')
        ->fields('n', array('nid'))
        ->condition('created', $timestamp, '<')
        ->execute();

        #TODO count how many nodes are going to be deleted
        $num_of_results = $result->rowCount();

        #TODO add an "are you sure?" validation step
        $confirm = drush_confirm('This will delete '.$num_of_results.' nodes. Proceed?');
        if ($confirm){
          foreach($result as $nid){
            drush_print('deleting '.$nid->nid);
            // print_r($nid);
            node_delete($nid->nid);
          }          
        }

      #TODO Consider other arguments, like node type (later)
      #TODO Add batch to prevent it from being too time consuming
      #TODO add option to delete older than [one year]

    } else {
      drush_print('Please provide a valid date');
    }      
  } else {
    drush_print('Please provide a date');
  }
}

function validateDate($date, $format = 'Y-m-d'){
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) === $date;
}
