Drush module

This module deletes all nodes older than a specified date

Use ``drush don yyyy-mm-dd``

(This project just contains the module files.
To see the complete lando development environment, look in project DB_Machete)